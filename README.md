# Unity Timeline Utilities
a collection Unity scripts to extend the unity timeline package.

## about
this is a a work in progress.
it's a growing collection of unity scripts that i develop for my own personal work.

## credits
nothing in here is solely my creation. most of it is at least partly inspired by what others publish online.
therefore i'll try to credit all of my references.
should i have missed something: i'm sorry and happy to add credit, where credit is due.

## license
creativecommons share-alike